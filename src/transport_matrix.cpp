///////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Eyeglasses-free Display: Towards Correcting Visual Aberrations with Computational Light Field Displays
// As individual project for "Displays"
// At the Eberhardt Karls Universität Tübingen, Germany
//
// This project tries to implement a computational display technology that predistorts
// the presented content for an observer, so that the target image is perceived without
// the need for eyewear. This light field displays based method was proposed by Huang et al.
// in their Paper "Eyeglasses-free Display: Towards Correcting Visual Aberrations with
// Computational Light Field Displays" (2014). The project is using the L-BFGS implementation
// of Dong C. Liu and Jorge Nocedal (chokkan.org) and the OpenCV 2.4.9 Open Source Computer Vision
// Library Bundle for the computation.
//
// Credit goes to Fu-Chung Huang, Gordon Wetzstein, Brian A. Barsky, Ramesh Raskar
//
// Author is Adrian Czarkowski
// See me also on Bitbucket, where this project is hosted:
// https://bitbucket.org/Atzeloth/vision-correcting-display
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////


/* Includes */
#include "../inc/transport_matrix.h"


cv::Mat linspace(float start, float end, int num_points)
{
	/* Initialize the result */
    cv::Mat array;

    /* Compute the stepsize */
    float stepsize = (end-start) / (num_points-1);

    /* Traverse the range with the stepsize */
    while(start <= end) {
        array.push_back(start);
        start += stepsize;
    }

    return array.t();
}


cv::Mat generateRange(float start, float stepsize, float end)
{
	/* Initialize the result */
	cv::Mat array;

	/* Traverse the range with the stepsize */
    while(start <= end) {
        array.push_back(start);
        start += stepsize;
    }

    return array.t();
}


cv::Mat angular_boundary(short num_view, float SPP, float depth, short extra_pix)
{
	if (num_view == 1)
	{
		cv::Mat AB(2, 1, CV_32FC1);
		AB.at<float>(0,0) = -90;
		AB.at<float>(1,0) = 90;
		return AB;
	}

	/* Boundaries == view + 1 on even views */
	if (num_view % 2 == 0)
	{
		/* Initialize the matrices */
		cv::Mat BVals(num_view * extra_pix / 2, 1, CV_32FC1);
		cv::Mat ud_BVals(num_view * extra_pix / 2, 1, CV_32FC1);
		cv::Mat AB;

		printf("INFO: Compute angular boundaries...\n");

		for (int i = 0; i < num_view * extra_pix / 2; i++)
		{
			printf("INFO: ... %3.2f %%\n", (i / (((float)num_view * (float)extra_pix) / 2)) * 100);

			BVals.at<float>(i,1) = atan(SPP * i / depth) / 3.14 * 180;
		}

		/* Flip the matrix upside down */
		cv::flip(BVals, ud_BVals, 1);

		/* Negate the flipped matrix */
		ud_BVals = ud_BVals * -1;

		/* Concatenate the matrices vertically */
		vconcat(ud_BVals, 0, AB);
		vconcat(AB, BVals, AB);

		/* Clean up */
		ud_BVals.release();
		BVals.release();

		return AB;
	}
	else
	{
		/* Initialize the matrices */
		cv::Mat BVals(floor(((float)num_view - 1) / 2) + 1 + num_view * ((float)extra_pix - 1) / 2, 1, CV_32FC1);
		cv::Mat ud_BVals(floor(((float)num_view - 1) / 2) + 1 + num_view * ((float)extra_pix - 1) / 2, 1, CV_32FC1);
		cv::Mat AB;

		printf("INFO: Compute angular boundaries...\n");

		for (int i = 0; i < floor(((float)num_view - 1) / 2) + 1 + num_view * ((float)extra_pix - 1) / 2; i++)
		{
			printf("INFO: ... %3.2f %%\n", (i / (floor(((float)num_view - 1) / 2) + 1 + num_view * ((float)extra_pix - 1) / 2)) * 100);
			BVals.at<float>(i,1) = atan((SPP / 2 + SPP * (i - 1)) / depth) / 3.14 * 180;
		}

		/* Flip the matrix upside down */
		cv::flip(BVals, ud_BVals, 1);

		/* Negate the flipped matrix */
		ud_BVals = ud_BVals * -1;

		/* Concatenate the matrices vertically */
		vconcat(ud_BVals, BVals, AB);

		/* Clean up */
		ud_BVals.release();
		BVals.release();

		return AB;
	}
}


void camera2object(cv::Mat X, cv::Mat Y, cv::Mat &x, cv::Mat &y)
{
	float delta = (float)1 / CAMERA_DI + (float)1 / DO - (float)1 / CAMERA_F;
	cv::Mat XY;

	/* Initialize a 2 x 2 matrix holding camera/eye specifics */
	cv::Mat T(2, 2, CV_32FC1);
	T.at<float>(0,0) = -(float)DO / CAMERA_DI;
	T.at<float>(0,1) = DO * delta;
	T.at<float>(1,0) = (float)1 / CAMERA_DI;
	T.at<float>(1,1) = (float)1 / CAMERA_F - (float)1 / CAMERA_DI;

	/* Concatenate the matrices vertically */
	vconcat(X.t(), Y.t(), XY);

	/* Let the rays pass the lens */
	cv::Mat R = T * XY;

	R.row(0).copyTo(x);
	R.row(1).copyTo(y);

	x.reshape(X.cols, X.rows);
	y.reshape(Y.cols, Y.rows);

	/* Clean up */
	R.release();
	T.release();
	X.release();
	Y.release();
	XY.release();
}


void backward_transport(float offset, cv::Mat BVals, short extra_pix, cv::Mat Vs, cv::Mat &Yo, cv::Mat &Vo)
{
	/* Initialize the matrices */
	Yo = cv::Mat::zeros(Vs.rows, SENSOR_RES, CV_32FC1);
	Vo = cv::Mat::zeros(Vs.rows, SENSOR_RES, CV_32FC1);
	cv::Mat S1 = linspace(-(float)SENSOR_WIDTH / 2, (float)SENSOR_WIDTH / 2, SENSOR_RES + 1);
	cv::Mat S2 = linspace(-(float)SCREEN_SIZE / 2, (float)SCREEN_SIZE / 2, SCREEN_RES + 1);
	cv::Mat ind_Yo = cv::Mat::zeros(Vs.rows, SENSOR_RES, CV_32FC1);
	cv::Mat sensor_sampling = cv::Mat::zeros(SENSOR_RES, 1, CV_32FC1);
	cv::Mat screen_sampling = cv::Mat::zeros(SCREEN_RES, 1, CV_32FC1);
	cv::Mat mask, K, S, Ys, x, y;

	/* Creates sensor and screen sample points */
	for (int i = 0; i < S1.rows - 1; i++)
	{
	    sensor_sampling.at<float>(i,0) = (S1.at<float>(0,i) + S1.at<float>(0,i+1)) / 2;
		screen_sampling.at<float>(i,0) = (S2.at<float>(0,i) + S2.at<float>(0,i+1)) / 2;
	}

	/* Clean up */
	S1.release();
	S2.release();

	printf("INFO: Compute flatland light field...\n");

	for (int j = 0; j < SENSOR_RES; j++)
	{
		printf("INFO: ... %3.2f %%\n", (j / (float)SENSOR_RES) * 100);

		/* Emit rays from the sensor  */
		Ys = cv::Mat::ones(Vs.cols, 1, CV_32FC1);
		Ys = Ys * sensor_sampling.at<float>(j);

		/* Propagate sensor rays to the screen */
		camera2object(Ys, Vs.t(), x, y);

		Yo.col(j) = x;
		Vo.col(j) = y;

		Vo.col(j) = Vo.col(j) / 3.14 * 180;
		Yo.col(j) = Yo.col(j) + offset;

		/* Initialize working variables */
		float min = 1.0/0.0; // 1.0/0.0 == inf
		int min_ind;

		/* Find the closest spatial pixel bucket between ray and screen sample */
		for (int x = 0; x < Yo.rows; x++)
		{
			for (int y = 0; y < screen_sampling.reshape(1).rows; y++)
			{
				if (min > sqrt((Yo.at<float>(x) - screen_sampling.at<float>(y)) * (Yo.at<float>(x) - screen_sampling.at<float>(y))))
				{
					min = sqrt((Yo.at<float>(x) - screen_sampling.at<float>(y)) * (Yo.at<float>(x) - screen_sampling.at<float>(y)));
					min_ind = y;
				}
			}

			ind_Yo.at<float>(x,j) = min_ind;
			min = 1.0/0.0;
		}

		S = Vo.col(j);

		/* Set angular values depending on angular resolution */
		if (SCREEN_ANGULAR_RES == 1)
		{
			K = cv::Mat::ones(S.rows, 1, CV_32FC1);
		}
		else
		{
			for (int a = 0; a < BVals.rows - 1; a++)
			{
				mask = ((S >= BVals.at<float>(a)) & (S < BVals.at<float>(a+1)));
				K.setTo(a - SCREEN_ANGULAR_RES * extra_pix, mask);
			}

			mask = ((S < BVals.at<float>(1)) | (S > BVals.at<float>(BVals.rows)));
			K.setTo(1.0/0.0, mask);
		}

		Vo.col(j) = K;
	}

	/* Amend pixel values depending on angular resolution */
	if (SCREEN_ANGULAR_RES != 1)
	{
		cv::Mat YM = cv::Mat::zeros(Yo.rows, Yo.cols, CV_32FC1);
		mask = (Vo <= SCREEN_ANGULAR_RES * (-extra_pix + 1));
		YM.setTo(- extra_pix, mask);
		Yo += YM;

		for (int i = -extra_pix+1; i < extra_pix; i++)
		{
			YM.setTo(cv::Scalar(0));
			mask = (Vo <= SCREEN_ANGULAR_RES * (i + 1)) & (Vo > SCREEN_ANGULAR_RES * i);
			YM.setTo(i, mask);
			Yo += YM;
		}

		YM.setTo(cv::Scalar(0));
		mask = (Vo > SCREEN_ANGULAR_RES * extra_pix);
		YM.setTo(extra_pix, mask);
		Yo += YM;

		printf("INFO: Amend pixel values depending on angular resolution...\n");

		for(int i = 0; i < Vo.rows; i++)
		{
		    for(int j = 0; j < Vo.cols; j++)
		    {
		    	printf("INFO: ... %3.2f %%\n", ((i * (float)Vo.rows + j) / ((float)Vo.rows * (float)Vo.cols)) * 100);

		    	Vo.at<float>(i,j) = ((int)(Vo.at<float>(i,j) + SCREEN_ANGULAR_RES - 1) % SCREEN_ANGULAR_RES) + 1;
		    }
		}
	}
}


cv::Mat transport_matrix(float eps_x, float eps_y)
{
	int psf_size = 240;
	short extra_pix = 5;
	int index = 1;

	/* Initialize matrices */
	cv::Mat ind_r = cv::Mat::zeros(SENSOR_RES * SENSOR_RES * psf_size * 2, 1, CV_32FC1);
	cv::Mat ind_c = cv::Mat::zeros(SENSOR_RES * SENSOR_RES * psf_size * 2, 1, CV_32FC1);
	cv::Mat val_s = cv::Mat::zeros(SENSOR_RES * SENSOR_RES * psf_size * 2, 1, CV_32FC1);
	cv::Mat Vs = generateRange(-(float)CAMERA_APERTURE / 2, (float)1 / SAMPLING, (float)CAMERA_APERTURE / 2);
	cv::Mat Us = generateRange(-(float)CAMERA_APERTURE / 2, (float)1 / SAMPLING, (float)CAMERA_APERTURE / 2);
	cv::Mat CELL, Yo, Vo, Xo, Uo, vs, us, vs2, us2, xo, yo, uo, vo;
	cv::Mat pixels, angles, samples, unique, ind, hist_ind;

	repeat(Vs, std::max(Vs.cols, Us.cols), 1, vs);
	repeat(Us, std::max(Vs.cols, Us.cols), 1, us);

	cv::Mat Z = cv::Mat::zeros(us.rows, us.cols, CV_32FC1);

	pow(us,2,us2);
	pow(vs,2,vs2);

	/* Set valid ray positions to 1 */
	cv::Mat mask = (us2 + vs2) < (((float)CAMERA_APERTURE / 2) * ((float)CAMERA_APERTURE / 2));
	Z.setTo(1, mask);

	/* Clean up */
	us.release();
	vs.release();
	us2.release();
	vs2.release();

	/* Get the horizontal and vertical angular boundaries for the given camera/eye parameters */
	cv::Mat HBVals = angular_boundary(SCREEN_ANGULAR_HRES, SCREEN_PIXEL_PITCH, SCREEN_DEPTH, extra_pix); printf("INFO: Computed horizontal angular boundary\n");
	cv::Mat VBVals = angular_boundary(SCREEN_ANGULAR_VRES, SCREEN_PIXEL_PITCH, SCREEN_DEPTH, extra_pix); printf("INFO: Computed vertical angular boundary\n");

	/* Compute the horizontal and vertical flatland light field */
	backward_transport(eps_y, HBVals, extra_pix, Vs, Yo, Vo); printf("INFO: Computed horizontal flatland light field\n");
	backward_transport(eps_x, VBVals, extra_pix, Us, Xo, Uo); printf("INFO: Computed vertical flatland light field\n");

	/* Sanity check */
	mask = Xo < 1;
	Xo.setTo(1, mask);
	mask = Xo > SCREEN_RES;
	Xo.setTo(SCREEN_RES, mask);

	/* Sanity check */
	mask = Yo < 1;
	Yo.setTo(1, mask);
	mask = Yo > SCREEN_RES;
	Yo.setTo(SCREEN_RES, mask);

	printf("INFO: Passed sanity checks\n");
	printf("INFO: Building projection matrix...\n");

	/* Start sampling and building projection matrix */
	for (int j = 0; j < SENSOR_RES; j++)
	{
		printf("INFO: ... %3.2f %%\n", (((j * (float)SENSOR_RES)) / ((float)SENSOR_RES * (float)SENSOR_RES)) * 100);

		for (int i = 0; i < SENSOR_RES; i++)
		{
			repeat(Xo.col(i), std::max(Xo.cols, Yo.cols), 1, xo);
			repeat(Yo.col(j), std::max(Xo.cols, Yo.cols), 1, yo);

			hconcat(xo.reshape(1), yo.reshape(1), pixels);

			repeat(Vo.col(i), std::max(Vo.cols, Uo.cols), 1, vo);
			repeat(Uo.col(j), std::max(Vo.cols, Uo.cols), 1, uo);

			hconcat(uo.reshape(1), vo.reshape(1), angles);

			hconcat(pixels, angles, samples);

			/* Remove samples that are blocked by the aperture */
			cv::Mat SM = cv::Mat::zeros(samples.rows, samples.cols, CV_32FC1);

			for (int e = 0; e < samples.rows; e++)
			{
				for (int r = 0; r < samples.cols; r++)
				{
					if (Z.reshape(1).at<float>(e) != 0)
					{
						SM.at<float>(e,r) = 1;
					}
				}
			}

			samples = samples.mul(SM);

			int w = samples.rows;

			unique.push_back(samples.row(0));
			ind.push_back(0);

			/* Accumulate the unique samples */
			for (int t = 1; t < samples.rows; t++)
			{
			    int inside = false;

			    for (int u = 0; u < unique.rows; u++)
			    {
			        int count = 0;

			        for (int k = 0; k < unique.cols; ++k)
			        {
			        	if(samples.at<float>(t,k) == unique.at<float>(u,k))
			        	{
			        		count++;
			        	}
			        }

			        if (count == samples.cols) {
			            inside = true;
			            ind.push_back(u);
			            break;
			        }
			    }

			    if (inside == false)
			    {
			    	unique.push_back(samples.row(i));
			    	ind.push_back(unique.rows-1);
			    }
			}

			double min, max;
			cv::minMaxLoc(ind, &min, &max);

			int count = 0;

			for (int v = 0; v < max; v++)
			{
				for (int i = 0; i < ind.rows; i++)
				{
					if (ind.at<float>(i,1) == v)
					{
						count++;
					}
				}

				hist_ind.push_back((float)count);
				count = 0;
			}

			CELL = unique;

			/* Prepare the sparse matrix */
			for (int r = 0; r < unique.rows; r++)
			{
				float index0 = (i - 1) * SENSOR_RES + (j - 1);
				float index1 = ((CELL.at<float>(r,1) - 1) * SCREEN_RES + (CELL.at<float>(r,1) - 1)) * SCREEN_ANGULAR_VRES * SCREEN_ANGULAR_HRES;
				float index2 = ((CELL.at<float>(r,3) - 1) * SCREEN_ANGULAR_VRES + (CELL.at<float>(r,3) - 1));

				ind_r.at<float>(index) = index0+1;
				ind_c.at<float>(index) = index1+index2+1;
				val_s.at<float>(index) = hist_ind.at<float>(r,0) / w;
				index++;
			}
		}
	}

	/* Initialize the sparse matrix */
	cv::Mat SM = cv::Mat::zeros(SENSOR_RES, SCREEN_RES * SCREEN_RES * SCREEN_ANGULAR_VRES * SCREEN_ANGULAR_HRES, CV_32FC1);

	printf("INFO: Filling sparse matrix...\n");

	/* Fill the sparse matrix properly */
	for (int k = 0; k < SM.rows * SM.cols; k++)
	{
		printf("INFO: ... %3.2f %%\n", (k / ((float)SM.rows * (float)SM.cols)) * 100);
		SM.at<float>(floor(ind_r.at<float>(k)), floor(ind_c.at<float>(k))) = val_s.at<float>(k);
	}

	return SM;
}
