///////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Eyeglasses-free Display: Towards Correcting Visual Aberrations with Computational Light Field Displays
// As individual project for "Displays"
// At the Eberhardt Karls Universität Tübingen, Germany
//
// This project tries to implement a computational display technology that predistorts
// the presented content for an observer, so that the target image is perceived without
// the need for eyewear. This light field displays based method was proposed by Huang et al.
// in their Paper "Eyeglasses-free Display: Towards Correcting Visual Aberrations with
// Computational Light Field Displays" (2014). The project is using the L-BFGS implementation
// of Dong C. Liu and Jorge Nocedal (chokkan.org) and the OpenCV 2.4.9 Open Source Computer Vision
// Library Bundle for the computation.
//
// Credit goes to Fu-Chung Huang, Gordon Wetzstein, Brian A. Barsky, Ramesh Raskar
//
// Author is Adrian Czarkowski
// See me also on Bitbucket, where this project is hosted:
// https://bitbucket.org/Atzeloth/vision-correcting-display
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////


/* Includes */
#include <stdio.h>
#include "../inc/transport_matrix.h"
#include "../lib/liblbfgs-1.10/include/lbfgs.h"


#define N 100


static cv::Mat img;
static cv::Mat TM;


static lbfgsfloatval_t evaluate(
    void *instance,
    const lbfgsfloatval_t *x,
    lbfgsfloatval_t *g,
    const int n,
    const lbfgsfloatval_t step
    )
{
    lbfgsfloatval_t fx = 0.0;

    for (int i = 0; i < n; i += 2)
    {
        lbfgsfloatval_t fcn = cv::norm(TM * x[i] - img, 2);
        fx += fcn;
    }

    return fx;
}


static int progress(void *instance,
    const lbfgsfloatval_t *x,
    const lbfgsfloatval_t *g,
    const lbfgsfloatval_t fx,
    const lbfgsfloatval_t xnorm,
    const lbfgsfloatval_t gnorm,
    const lbfgsfloatval_t step,
    int n,
    int k,
    int ls
    )
{
    printf("Iteration %d:\n", k);
    printf("  fx = %f, x[0] = %f, x[1] = %f\n", fx, x[0], x[1]);
    printf("  xnorm = %f, gnorm = %f, step = %f\n", xnorm, gnorm, step);
    printf("\n");

    return 0;
}


int main(int argc, char* argv[])
{
	/* Read in the image */
	img = cv::imread("img/0084.png", CV_LOAD_IMAGE_UNCHANGED);

	/* Vectorize the image */
	img.reshape(1);

	/* Get the transport matrix if already computed */
	cv::FileStorage fsr; fsr.open("tm.xml", cv::FileStorage::READ);
	fsr["TM"] >> TM; fsr.release();

	/* Compute the transport matrix if needed */
	if (TM.empty())
    {
		printf("No transport matrix found. Will compute it now...\n");

		/* Compute the transport matrix */
		TM = transport_matrix(0,0);

		printf("Computed transport matrix\n");

	    // Save the transport matrix
		cv::FileStorage fsw("tm.xml", cv::FileStorage::WRITE);
		fsw << "TM" << TM; fsw.release();
    }

	/* Compute the screen side light field */
    int ret = 0;
    lbfgsfloatval_t fx;
    lbfgsfloatval_t *x = lbfgs_malloc(N);
    lbfgs_parameter_t param;

    if (x == NULL)
    {
        printf("ERROR: Failed to allocate a memory block for variables.\n");
        return 1;
    }

    /* Initialize the variables. */
    for (int i = 0; i < N; i += 2)
    {
        x[i] = -1.2;
        x[i+1] = 1.0;
    }

    /* Initialize the parameters for the L-BFGS optimization */
    lbfgs_parameter_init(&param);

    /*
     * Start the L-BFGS optimization; this will invoke the
     * callback functions evaluate() and progress() when necessary
     */
    ret = lbfgs(N, x, &fx, evaluate, progress, NULL, &param);

    /* Report the result */
    printf("L-BFGS optimization terminated with status code = %d\n", ret);
    printf("  fx = %f, x[0] = %f, x[1] = %f\n", fx, x[0], x[1]);

    /* Clean up */
    lbfgs_free(x);

	return 0;
}
