///////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Eyeglasses-free Display: Towards Correcting Visual Aberrations with Computational Light Field Displays
// As individual project for "Displays"
// At the Eberhardt Karls Universität Tübingen, Germany
//
// This project tries to implement a computational display technology that predistorts
// the presented content for an observer, so that the target image is perceived without
// the need for eyewear. This light field displays based method was proposed by Huang et al.
// in their Paper "Eyeglasses-free Display: Towards Correcting Visual Aberrations with
// Computational Light Field Displays" (2014). The project is using the L-BFGS implementation
// of Dong C. Liu and Jorge Nocedal (chokkan.org) and the OpenCV 2.4.9 Open Source Computer Vision
// Library Bundle for the computation.
//
// Credit goes to Fu-Chung Huang, Gordon Wetzstein, Brian A. Barsky, Ramesh Raskar
//
// Author is Adrian Czarkowski
// See me also on Bitbucket, where this project is hosted:
// https://bitbucket.org/Atzeloth/vision-correcting-display
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////


#ifndef SCREEN_H_
#define SCREEN_H_


#define SCREEN_DEPTH			5.514
#define SCREEN_ANGULAR_RES		5
#define SCREEN_ANGULAR_HRES		SCREEN_ANGULAR_RES
#define SCREEN_ANGULAR_VRES		SCREEN_ANGULAR_RES
#define SCREEN_PIXEL_PITCH		(78 * 0.001)
#define SCREEN_PITCH			(SCREEN_ANGULAR_RES * SCREEN_PIXEL_PITCH)
#define SCREEN_PADDING			12
#define SCREEN_PIXELS			128
#define SCREEN_RES				(SCREEN_PIXELS + SCREEN_PADDING)
#define SCREEN_SIZE				(SCREEN_RES * SCREEN_PITCH)


#endif /* SCREEN_H_ */
