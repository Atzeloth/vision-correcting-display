///////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Eyeglasses-free Display: Towards Correcting Visual Aberrations with Computational Light Field Displays
// As individual project for "Displays"
// At the Eberhardt Karls Universität Tübingen, Germany
//
// This project tries to implement a computational display technology that predistorts
// the presented content for an observer, so that the target image is perceived without
// the need for eyewear. This light field displays based method was proposed by Huang et al.
// in their Paper "Eyeglasses-free Display: Towards Correcting Visual Aberrations with
// Computational Light Field Displays" (2014). The project is using the L-BFGS implementation
// of Dong C. Liu and Jorge Nocedal (chokkan.org) and the OpenCV 2.4.9 Open Source Computer Vision
// Library Bundle for the computation.
//
// Credit goes to Fu-Chung Huang, Gordon Wetzstein, Brian A. Barsky, Ramesh Raskar
//
// Author is Adrian Czarkowski
// See me also on Bitbucket, where this project is hosted:
// https://bitbucket.org/Atzeloth/vision-correcting-display
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////


#ifndef SENSOR_H_
#define SENSOR_H_


/* Includes */
#include "screen.h"


/* Choose a mode */
#define CAMERA 1
//#define MYOPIA 1
//#define HYPEROPIA 1


#ifdef CAMERA
#define CAMERA_F				50 // mm
#define CAMERA_FSTOP			8
#define CAMERA_APERTURE			(CAMERA_F / CAMERA_FSTOP)
#define CAMERA_FOCUS			375 // mm
#define CAMERA_DI				(CAMERA_F * CAMERA_FOCUS / (CAMERA_FOCUS - CAMERA_F))
#endif

#ifdef MYOPIA
#define CAMERA_APERTURE			5
#define DIOPTER_RELAX			60
#define CAMERA_DI				(1 / DIOPTER_RELAX * 1000)
#define MYOPIA_DIOPTER			-3
#define DIOPTER					(DIOPTER_RELAX - MYOPIA_DIOPTER)
#define CAMERA_F				(1 / DIOPTER * 1000)
#define CAMERA_DO_STAR			((CAMERA_F * CAMERA_DI) / (CAMERA_DI - CAMERA_F))
#endif

#ifdef HYPEROPIA
#define CAMERA_APERTURE			5
#define DIOPTER_RELAX			60
#define CAMERA_DI				(1 / DIOPTER_RELAX * 1000)
#define HYPEROPIA_DIOPTER		5.775
#define CRYSTALLENS_POWER		8
#define DIOPTER					(DIOPTER_RELAX + CRYSTALLENS_POWER - HYPEROPIA_DIOPTER)
#define CAMERA_F				(1 / DIOPTER * 1000)
#define CAMERA_DO_STAR			((CAMERA_F * CAMERA_DI) / (CAMERA_DI - CAMERA_F))
#endif

#define DO						250
#define SAMPLING				20
#define SENSOR_RES				128
#define SENSOR_WIDTH			(SCREEN_PIXELS * SCREEN_PITCH / DO * CAMERA_DI)
#define SENSOR_PITCH			(SENSOR_WIDTH / SENSOR_RES)


#endif /* SENSOR_H_ */
