///////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Eyeglasses-free Display: Towards Correcting Visual Aberrations with Computational Light Field Displays
// As individual project for "Displays"
// At the Eberhardt Karls Universität Tübingen, Germany
//
// This project tries to implement a computational display technology that predistorts
// the presented content for an observer, so that the target image is perceived without
// the need for eyewear. This light field displays based method was proposed by Huang et al.
// in their Paper "Eyeglasses-free Display: Towards Correcting Visual Aberrations with
// Computational Light Field Displays" (2014). The project is using the L-BFGS implementation
// of Dong C. Liu and Jorge Nocedal (chokkan.org) and the OpenCV 2.4.9 Open Source Computer Vision
// Library Bundle for the computation.
//
// Credit goes to Fu-Chung Huang, Gordon Wetzstein, Brian A. Barsky, Ramesh Raskar
//
// Author is Adrian Czarkowski
// See me also on Bitbucket, where this project is hosted:
// https://bitbucket.org/Atzeloth/vision-correcting-display
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////


#ifndef TRANSPORT_MATRIX_H_
#define TRANSPORT_MATRIX_H_


/* Includes */
#include "../inc/sensor.h"
#include "../inc/screen.h"
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/features2d.hpp>


/**
 * @brief OpenCV based C++ equivalent to Matlabs 'linspace'
 * @detail Creates a given amount of evenly spaced points
 * 		   between the start value and the end value
 * @param start 	 The start value
 * @param end		 The end value
 * @param num_points The amount of points to create
 * @return A 1 x n matrix holding the n points
 */
cv::Mat linspace(float start, float end, int num_points);


/**
 * @brief OpenCV based C++ equivalent to Matlabs colon operator for ranges
 * @detail Creates as much points with distance stepsize
 * 		   between the start value and the end value
 * @param start 	 The start value
 * @param stepsize   The stepsize
 * @param end		 The end value
 * @return A 1 x n matrix holding the n points
 */
cv::Mat generateRange(float start, float stepsize, float end);


/**
 * @brief Computes the angular boundaries for a given number of views
 * @detail Given the amount of views, the depth and the amounts of
 * 		   additional pixels to consider the viewing angles are computed
 * @param num_view	The angular resolution of the display
 * @param SPP		The screen pixel pitch
 * @param depth		The display depth
 * @param extra_pix The amount of surrounding pixels to consider
 * @return A matrix holding the viewing angles
 */
cv::Mat angular_boundary(short num_view, float SPP, float depth, short extra_pix);


/**
 * @brief Propagates rays from the camera to an object
 * @detail The function propagates rays from the given sensor
 * 		   points to the screen using camera/eye specifics
 * @param X 	A n x 1 matrix holding x-values of sensor points
 * @param Y 	A n x 1 matrix holding y-values of sensor points
 * @param x 	A n x 1 matrix holding x-values of screen points
 * @param y		A n x 1 matrix holding y-values of screen points
 */
void camera2object(cv::Mat X, cv::Mat Y, cv::Mat &x, cv::Mat &y);


/**
 * @brief Computes the flatland light field on the screen side
 * @detail Given the sensor side light field formed by the spatial and angular
 * 		   samplings the function propagates the rays from the sensor to the screen.
 * 		   The screen side light field is then converted to an index-based format
 * @param offset	The value offset
 * @param BVals		The angular boundaries
 * @param extra_pix	The amount of surrounding pixels to consider
 * @param Vs		The sensor points
 * @param Yo		The light field pixel values
 * @param Vo		The light field angle values
 */
void backward_transport(float offset, cv::Mat BVals, short extra_pix, cv::Mat Vs, cv::Mat &Yo, cv::Mat &Vo);


/**
 * @brief Generates the light field transport matrix
 * @detail Given the transported screen side light field the function counts
 * 		   the samples and generates the light field transport matrix that
 * 		   relates the screen side light to the received image on the sensor
 * @param eps_x		The offset in x direction
 * @param eps_y		The offset in y direction
 * @return The sparse transport matrix
 */
cv::Mat transport_matrix(float eps_x, float eps_y);


#endif /* TRANSPORT_MATRIX_H_ */
