# Readme #

### 1. Preamble ###

    Eyeglasses-free Display: Towards Correcting Visual Aberrations with Computational Light Field Displays
    As individual project for "Displays"
    At the Eberhardt Karls Universitaet Tuebingen, Germany

          This project tries to implement a computational display technology that predistorts
          the presented content for an observer, so that the target image is perceived without
          the need for eyewear. This light field displays based method was proposed by Huang et al.
          in their Paper "Eyeglasses-free Display: Towards Correcting Visual Aberrations with
          Computational Light Field Displays" (2014).


### 2. Compilation ###

    To compile the project download the code.
    You will need to install:

        - The C/C++ Computer Vision Library Bundle OpenCV (3.0.0)
          which is a BSD licensed standalone Software
          -> http://opencv.org/



### 3. Author ###

    Author is Adrian Czarkowski



### 4. Credits ###

    Credit goes to Fu-Chung Huang, Gordon Wetzstein, Brian A. Barsky, Ramesh Raskar for their paper,
    to Dong C. Liu and Jorge Nocedal for their used code (chokkan.org) to provide a L-BFGS solver
    and of course to all the provided libraries.



### 5. Sources ###

    The following sources were used:
    - OpenCV